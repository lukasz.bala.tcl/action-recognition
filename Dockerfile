# This image can run with or without GPUs
FROM nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04

# Disable interactive functions
ENV DEBIAN_FRONTEND noninteractive

# Basic stuff
RUN apt-get update --fix-missing && \
    apt-get install -y \
        locales \
        wget \
        bzip2 \
        sudo \
        git \
        zip \
        curl \
        htop \
        iputils-ping\
        net-tools \
        tmux \
        libsm6 libxext6 libxrender-dev \
        python3 \
        python3-pip && \
        rm -rf /var/lib/apt/lists/*

RUN apt-get update \
    && apt-get install software-properties-common -y \
    && add-apt-repository -y ppa:deadsnakes/ppa \
    && apt-get update \
    && apt-get install python3.7 -y \
    && update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2 \
    && apt install python3.7-dev -y \
    && apt install python3 python-dev python3-dev \
        build-essential libssl-dev libffi-dev \
        libxml2-dev libxslt1-dev zlib1g-dev \
        python-pip -y

# Fix UTF
# https://stackoverflow.com/a/38553499/1407427
RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8
ENV LANG en_US.UTF-8

# Setup user (will get id 1000). Add him to proper groups and set password
RUN useradd -ms /bin/bash tclbot; usermod -aG users,sudo,adm tclbot; echo "tclbot:tcl" | chpasswd
# Now, run as a new user
USER tclbot
COPY requirements.txt /home/tclbot
WORKDIR /home/tclbot
# Install basic python stack
RUN pip3 install --upgrade pip && \
    pip3 install --upgrade Cython && \
    pip3 install --upgrade numpy && \
    pip3 install -r requirements.txt\
     --no-cache-dir

#!/usr/bin/env bash

if [[ "$1" == "" ]]; then
  echo "Please provide name of the job."
  exit 1
fi

if [[ "$2" == "" ]]; then
  echo "Please provide file name with command."
  exit 1
fi

if [[ "$2" == "human-pose"]]; then
  SUB_PROJECT_NAME = "human-pose-estimation"
  COMMAND="python eval.py STANDARD_RGB --model_path ${MODEL_PATH} --dataset_type ${DATASET_TYPE}"
elif [[ "$2" == "real-time"]]; then
  SUB_PROJECT_NAME = "real-time-action"
elif [[ "$2" == "real-time"]]; then
  SUB_PROJECT_NAME = "real-time-action"
  COMMAND="python3 run_video_skeleton.py --model=mobilenet_thin --resize=432x368 --video_folder ./videos/brushing_teeth/"
elif [[ "$2" == "tf-pose-estimation"]]; then
  SUB_PROJECT_NAME = "human-pose-estimation/src/githubs/tf-pose-estimation"
  COMMAND="python3 run_video_skeleton.py --model=mobilenet_thin --resize=432x368 --video_folder ./videos/brushing_teeth/"
else
  echo "Wrong subproject name"
  exit 1
fi
envsubst < command.sh

. command.sh

envsubst < job.yml | kubectl create -f -

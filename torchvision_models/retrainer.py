import torchvision.models as models
import pytorch_lightning as pl

class VideoTransferLearning(pl.LightningModule):
    def __init__(self, num_target_classes, feature_extractor):
        # init a pretrained resnet
        self.num_target_classes = num_target_classes
        self.feature_extractor = feature_extractor(
                                    pretrained=True,
                                    num_classes=num_target_classes)
        self.feature_extractor.eval()

        # use the pretrained model to classify cifar-10 (10 image classes)
        self.classifier = nn.Linear(2048, num_target_classes)

    def forward(self, x):
        representations = self.feature_extractor(x)
        x = self.classifier(representations)
        return x